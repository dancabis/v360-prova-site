import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

export type SearchResult = {
  ean: number,
  name: string,
  probability: number
};

@Injectable({
  providedIn: 'root'
})
export class ProductSearchService {

  constructor(private client: HttpClient) { }

  searchByDescription(description: string): Observable<SearchResult> {
    const url = environment.serverUrl + '/products';
    const options = {
      params: new HttpParams().set('description', description)
    };
    return this.client.get<SearchResult>(url, options);
  }

}
