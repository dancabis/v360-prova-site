import { Component, OnInit } from '@angular/core';

import { ProductSearchService, SearchResult } from '../product-search.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {

  description: string;
  result: SearchResult;
  error: string;

  constructor(private productSearch: ProductSearchService) { }

  ngOnInit() {
  }

  search() {
    this.result = null;
    this.error = null;
    this.productSearch.searchByDescription(this.description)
      .subscribe(
        val => this.result = val,
        error => this.error = `Não foi possível obter resposta.
          Por favor, verifique sua conexão.`,
      );
  }

}
